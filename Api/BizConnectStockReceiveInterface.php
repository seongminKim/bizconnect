<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-06
 * Time: 오전 11:03
 */

namespace Eguana\BizConnect\Api;

/**
 * BizConnect Stock Receive
 *
 * @api
 */
interface BizConnectStockReceiveInterface
{
    /**
     * Save Stock data to bizConnect table
     * @return mixed
     */
    public function saveStockBizData();
}