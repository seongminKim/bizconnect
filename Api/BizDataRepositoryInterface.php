<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 1:59
 */

namespace Eguana\BizConnect\Api;

use Eguana\BizConnect\Api\Data\BizDataInterface;
use Magento\Cms\Api\Data\BlockSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;

interface BizDataRepositoryInterface
{

    /**
     * @param BizDataInterface $bizData
     * @return mixed
     */
    public function save(BizDataInterface $bizData);

    /**
     * @param $entity_id
     * @return mixed
     */
    public function getById($entity_id);

    /**
     * Retrieve blocks matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return BlockSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param BizDataInterface $bizData
     * @return mixed
     */
    public function delete(BizDataInterface $bizData);

    /**
     * @param $entity_id
     * @return mixed
     */
    public function deleteById($entity_id);
}