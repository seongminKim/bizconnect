<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-06
 * Time: 오전 10:52
 */

namespace Eguana\BizConnect\Api;

/**
 * BizConnect Product Receive
 *
 * @api
 */
interface BizConnectProductReceiveInterface
{
    /**
     * Save product data to bizConnect table
     * @return mixed
     */
    public function saveProductBizData();
}