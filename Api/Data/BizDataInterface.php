<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 1:40
 */

namespace Eguana\BizConnect\Api\Data;

interface BizDataInterface
{
    const ENTITY_ID = 'entity_id';

    const WEBSITE_ID = 'website_id';

    const TYPE = 'type';

    const BIZ_DATA = 'biz_data';

    const STATUS = 'status';

    const MESSAGE = 'message';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param $website_id
     * @return $this
     */
    public function setWebsiteId($website_id);

    /** @return string*/
    public function getWebsiteId();

    /**
     * @param $type
     * @return $this
     */
    public function setType($type);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param $biz_data string
     * @return $this
     */
    public function setBizData($biz_data);

    /** @return string*/
    public function getBizData();

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status);

    /** @return string*/
    public function getStatus();

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message);

    /** @return string*/
    public function getMessage();

    /**
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at);

    /**
     * @return string
     */
    public function getUpdatedAt();
}
