<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 2:09
 */

namespace Eguana\BizConnect\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface BizDataSearchResultInterface extends SearchResultsInterface
{

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return SearchResultsInterface
     */
    public function setItems(array $items);
}