<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-02
 * Time: 오후 1:24
 */

namespace Eguana\BizConnect\Model\Order;

use Exception;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\ExceptionCode;
use Eguana\BizConnect\Api\Data\BizDataInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;

class OrderDataIntegration extends ExceptionCode
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var BizDataInterfaceFactory
     */
    private $bizDataInterfaceFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var BizDataRepositoryInterface
     */
    private $bizDataRepository;

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderRepositoryInterface $orderRepository,
        FilterBuilder $filterBuilder,
        OrderItemRepositoryInterface $orderItemRepository,
        Json $json,
        BizDataInterfaceFactory $bizDataInterfaceFactory,
        StoreManagerInterface $storeManager,
        BizDataRepositoryInterface $bizDataRepository
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderRepository = $orderRepository;
        $this->filterBuilder = $filterBuilder;
        $this->orderItemRepository = $orderItemRepository;
        $this->json = $json;
        $this->bizDataInterfaceFactory = $bizDataInterfaceFactory;
        $this->storeManager = $storeManager;
        $this->bizDataRepository = $bizDataRepository;
    }

    public function bindOrderData()
    {
        $itemData = $this->getItemData();
        $bindData = [];

        if ($itemData['count'] > 0) {
            $items = $itemData['data'];

            foreach ($items as $item) {
                $itemId = $item->getItemId();
                $sku = $item->getSku();
                $name = $item->getName();
                $itemPrice = $item->getPrice();
                $itemQtyOrdered = (int)$item->getQtyOrdered();

                $bindData[] = [
                    'item_id' => $itemId,
                    'sku' => $sku,
                    'name' => $name,
                    'price' => $itemPrice,
                    'qty' => $itemQtyOrdered
                ];
            }
        }

        return $bindData;
    }

    private function getItemData()
    {
        $orderStateFilter = $this->searchCriteriaBuilder->addFilter('state', 'complete')->create();
        $orders = $this->orderRepository->getList($orderStateFilter)->getItems();

        $orderIdFilter = [];
        foreach ($orders as $order) {
            $orderIdFilter[] = $this->filterBuilder
                ->setField('order_id')
                ->setConditionType('eq')
                ->setValue($order->getId())->create();
        }

        if (!empty($orderIdFilter)) {
            $itemData = $this->getItemSearchCriteria($orderIdFilter);
        } else {
            $itemData['data'] = '';
            $itemData['count'] = 0;
        }

        return $itemData;
    }

    private function getItemSearchCriteria($orderIdFilter)
    {
        $searchItemFilter = $this->searchCriteriaBuilder->addFilters($orderIdFilter);
        $searchItems = $searchItemFilter->addFilter('interface_status_item', '00')->create();
        $itemData['data'] = $this->orderItemRepository->getList($searchItems)->getItems();
        $itemData['count'] = $this->orderItemRepository->getList($searchItems)->getTotalCount();

        return $itemData;
    }

    public function saveBizData($bindData)
    {
        if (isset($bindData)) {
            $message =[];

            foreach ($bindData as $key => $value) {
                $bizDataFactory = $this->bizDataInterfaceFactory->create();
                try {
                    $bizDataFactory->setType('order');
                    $bizDataFactory->setWebsiteId($this->storeManager->getStore()->getWebsiteId());
                    $bizDataFactory->setBizData($this->json->serialize($value));
                    $this->bizDataRepository->save($bizDataFactory);

                    $message[$value['item_id']] = 'success';
                } catch (\Exception $e) {
                    $message[$value['item_id']] ='Order Item ID '.$value['item_id'].' error.' . $e->getMessage();
                    continue;
                }
            }
            return $message;
        }
    }

    public function updateOrderItemStatus($orderData)
    {
        $result = [];

        foreach ($orderData as $entity_id => $order) {
            try {
                $order_item = $this->orderItemRepository->get($order['item_id']);
                $order_item->setData('interface_status_item', '01');

                $this->orderItemRepository->save($order_item);
                $result[] = ['result' => true, 'Message' => 'SUCCESS'];
            } catch (Exception $e) {
                $result[] = ['result' => false, 'Message' => $e->getMessage()];
            }
        }
        return $result;
    }

    /**
     * @param $result
     * @param $orderData
     * @throws CouldNotSaveException
     */
    public function updateBizStatus($result, $orderData)
    {
        foreach ($orderData as $entity_id => $order) {
            if ($result['code'] == '0000') {
                try {
                    $bizData = $this->bizDataRepository->getById($entity_id);
                    $bizData->setData('status', "01");
                    $bizData->setData('message', 'SUCCESS');
                    $this->bizDataRepository->save($bizData);
                } catch (Exception $exception) {
                    throw new CouldNotSaveException(
                        __('Could not update status : %1', $exception->getMessage()),
                        $exception
                    );
                }
            } else {
                $bizData = $this->bizDataRepository->getById($entity_id);
                $bizData->setData('status', "02");
                $bizData->setData('message', 'FAIL');
                $this->bizDataRepository->save($bizData);
            }
        }
    }
}