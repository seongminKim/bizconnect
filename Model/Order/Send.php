<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-10
 * Time: 오전 10:25
 */

namespace Eguana\BizConnect\Model\Order;

use Eguana\BizConnect\Model\ExceptionCode;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\HTTP\Client\Curl;

class Send extends ExceptionCode
{
    /**
     * @var Json
     */
    private $json;
    /**
     * @var Curl
     */
    private $curl;

    public function __construct(
        Json $json,
        Curl $curl
    ) {
        $this->json = $json;
        $this->curl = $curl;
    }

    public function sendData($requestData)
    {
//        $url = 'http://192.168.0.166/rest/all/V1/bizConnect/order';
//        $this->curl->addHeader('Content-Type', 'application/json');
//        $this->curl->post($url, $requestData);
//
//        $response = $this->curl->getBody();
        $response = '{"code" : "0000",
"message" : "SUCCESS",
"data" : true}
';
        $result = $this->json->unserialize($response);

        if ($result['code'] == '0000') {
            $result_message = $result;
        } else {
            $exception_message = $this->exceptionResult($result['code']);
            $result_message = ['code' => $result['code'], 'message' => $exception_message];
        }

        return $result_message;
    }
}