<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 1:45
 */

namespace Eguana\BizConnect\Model;

use Magento\Framework\Model\AbstractModel;
use Eguana\BizConnect\Api\Data\BizDataInterface;

class BizData extends AbstractModel implements BizDataInterface
{
    public function _construct()
    {
        $this->_init(\Eguana\BizConnect\Model\ResourceModel\BizData::class);
    }

    /**
     * @param $website_id
     * @return mixed
     */
    public function setWebsiteId($website_id)
    {
        return $this->setData(self::WEBSITE_ID, $website_id);
    }

    /** @return string */
    public function getWebsiteId()
    {
        return $this->getData(self::WEBSITE_ID);
    }

    /**
     * @param $type
     * @return mixed
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @param $biz_data string
     * @return $this
     */
    public function setBizData($biz_data)
    {
        return $this->setData(self::BIZ_DATA, $biz_data);
    }

    /** @return string */
    public function getBizData()
    {
        return $this->getData(self::BIZ_DATA);
    }

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /** @return string */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param $message
     * @return mixed
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /** @return string */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * @param string $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @param string $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {
        return $this->setData(self::UPDATED_AT, $updated_at);
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }
}