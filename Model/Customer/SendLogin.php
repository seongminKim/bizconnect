<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-03
 * Time: 오후 3:56
 */

namespace Eguana\BizConnect\Model\Customer;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\ExceptionCode;
use Eguana\BizConnect\Model\Authorize\Token;
use Magento\Customer\Model\Session;

class SendLogin extends ExceptionCode
{
    const CUSTOMER_LOGIN_URL = 'https://commerce.apdigit.com/v1.0/member';

    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var Token
     */
    private $token;
    /**
     * @var Session
     */
    private $session;

    public function __construct(
        Curl $curl,
        Json $json,
        Token $token,
        Session $session
    ) {
        $this->curl = $curl;
        $this->json = $json;
        $this->token = $token;
        $this->session = $session;
    }

    public function sendLoginData()
    {
        $token_result = $this->token->callTokenApi();

        if ($token_result['code'] = '0000') {
//            $customer_token = $this->session->getAuthorizeToken();
//            $this->curl->addHeader('Authorization', 'Bearer '.$customer_token);
//            $this->curl->get(self::CUSTOMER_LOGIN_URL);

//        $response = $this->curl->getBody();
            $response = '{
"code" : "0000",
"message" : "SUCCESS",
"data" : {
"member_no" : 10001,
"email" : "qwd@casd.com",
"first_name" : "aaa",
"last_name" : "aaa"
}
}';
            $result = $this->json->unserialize($response);

            if ($result['code'] == '0000') {
                $result_message = $result;
            } else {
                $exception_message = $this->exceptionResult($result['code']);
                $result_message = ['code' => $result['code'], 'message' => $exception_message];
            }

            return $result_message;
        } else {
            return $token_result;
        }
    }
}