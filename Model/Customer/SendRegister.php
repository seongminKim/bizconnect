<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-29
 * Time: 오전 10:59
 */

namespace Eguana\BizConnect\Model\Customer;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\ExceptionCode;

class SendRegister extends ExceptionCode
{
    /**
     * @var Curl
     */
    public $curl;
    /**
     * @var Json
     */
    private $json;

    public function __construct(
        Curl $curl,
        Json $json
    ) {
        $this->curl = $curl;
        $this->json = $json;
    }

    public function sendCustomerData($registerData)
    {
//        $url = 'https://commerce.apdigit.com/v1.0/member';

//        $jsonRegisterData = $this->json->serialize($registerData);

//        $this->curl->post($url, $jsonRegisterData);

//        $response = $this->curl->getBody();
        $response = '{
"code" : "1100",
"message" : "SUCCESS",
"data" : {
"member_no" : 10001,
"email" : "t15123112@tt.com",
"first_name" : "t11232est1",
"last_name" : "ki12312m1"
}
}
';
        $result = $this->json->unserialize($response);

        if ($result['code'] == '0000') {
            $result_message = $result;
        } else {
            $exception_message = $this->exceptionResult($result['code']);
            $result_message = ['code' => $result['code'], 'message' => $exception_message];
        }

        return $result_message;
    }
}