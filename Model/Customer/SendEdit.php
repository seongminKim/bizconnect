<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-03
 * Time: 오후 2:34
 */

namespace Eguana\BizConnect\Model\Customer;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\ExceptionCode;
use Magento\Customer\Model\Session;

class SendEdit extends ExceptionCode
{
    const CUSTOMER_EDIT_URL = 'https://commerce.apdigit.com/v1.0/member';
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var Session
     */
    private $session;

    public function __construct(
        Curl $curl,
        Json $json,
        Session $session
    ) {
        $this->curl = $curl;
        $this->json = $json;
        $this->session = $session;
    }

    public function sendEditData($editData)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('session edit : '. $this->session->getAuthorizeToken());

//        $customer_token = $this->session->getAuthorizeToken();

//        $jsonEditData = $this->json->serialize($editData);

//        $this->curl->addHeader('Authorization', 'Bearer '.$customer_token);
//        $this->curl->post(self::CUSTOMER_EDIT_URL, $jsonEditData);

//        $response = $this->curl->getBody();
        $response = '{
"code" : "0000",
"message" : "SUCCESS",
"data" : {
"member_no" : 10001,
"email" : "aaa124124@tt.com",
"first_name" : "aaa",
"last_name" : "aaa"
}
}';
        $result = $this->json->unserialize($response);

        if ($result['code'] == '0000') {
            $result_message = $result;
        } else {
            $exception_message = $this->exceptionResult($result['code']);
            $result_message = ['code' => $result['code'], 'message' => $exception_message];
        }

        return $result_message;
    }
}