<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-04
 * Time: 오전 9:11
 */

namespace Eguana\BizConnect\Model\Rma;

use Exception;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Rma\Model\ResourceModel\Item\CollectionFactory;
use Eguana\BizConnect\Api\Data\BizDataInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Model\ExceptionCode;

class RmaDataIntegration extends ExceptionCode
{
    /**
     * @var CollectionFactory
     */
    private $rmaItemCollectionFactory;
    /**
     * @var BizDataInterfaceFactory
     */
    private $bizDataInterfaceFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var BizDataRepositoryInterface
     */
    private $bizDataRepository;

    public function __construct(
        CollectionFactory $rmaItemCollectionFactory,
        BizDataInterfaceFactory $bizDataInterfaceFactory,
        StoreManagerInterface $storeManager,
        Json $json,
        BizDataRepositoryInterface $bizDataRepository
    ) {
        $this->rmaItemCollectionFactory = $rmaItemCollectionFactory;
        $this->bizDataInterfaceFactory = $bizDataInterfaceFactory;
        $this->storeManager = $storeManager;
        $this->json = $json;
        $this->bizDataRepository = $bizDataRepository;
    }

    public function bindRmaData()
    {
        $rmaItemCollection = $this->rmaItemCollectionFactory->create();
        $rmaItemCollection->addAttributeToSelect('*');
        $rmaItemCollection->getSelect()->where('e.status = ?', 'authorized');
        $rmaItemCollection->getSelect()->where('e.interface_status_item = ?', '00');

        $bindData = [];

        foreach ($rmaItemCollection as $item) {
            $order = $item->getRma()->getOrder();

            $bindData[] = [
                'increment_id' => $order->getIncrementId(),
                'item_id' => $item->getOrderItemId(),
                'sku' => $item->getProductAdminSku(),
                'product_nam' => $item->getProductAdminName(),
                'qty_requested' => $item->getQtyRequested()
            ];
        }
        return $bindData;
    }

    public function saveBizData($bindData)
    {
        if (isset($bindData)) {
            $message =[];
            foreach ($bindData as $key => $value) {
                $bizDataFactory = $this->bizDataInterfaceFactory->create();
                try {
                    $bizDataFactory->setType('rma');
                    $bizDataFactory->setWebsiteId($this->storeManager->getStore()->getWebsiteId());
                    $bizDataFactory->setBizData($this->json->serialize($value));
                    $this->bizDataRepository->save($bizDataFactory);

                    $message[$value['item_id']] = 'success';
                } catch (\Exception $e) {
                    $message[$value['item_id']] ='RMA Item ID '.$value['item_id'].' error.' . $e->getMessage();
                    continue;
                }
            }
            return $message;
        }
    }

    public function updateRmaItemStatus($rmaData)
    {
        $result = [];

        foreach ($rmaData as $entity_id => $rma) {
            try {
                $rmaItemCollection = $this->rmaItemCollectionFactory->create();
                $rmaItemCollection->addAttributeToSelect('*');
                $rmaItemCollection->getSelect()->where('e.status = ?','authorized');
                $rmaItemCollection->getSelect()->where('e.interface_status_item = ?','00');
                $rmaItemCollection->getSelect()->where('e.order_item_id = ?', $rma['item_id']);

                $rmaItemCollection->getResource()
                    ->getConnection()
                    ->update(
                        $rmaItemCollection->getTable('magento_rma_item_entity'),
                        ['interface_status_item' => '01'],
                        ['order_item_id' => $rma['item_id']]);

                $result[] = ['result' => true, 'Message' => 'SUCCESS'];
            } catch (Exception $e) {
                $result[] = ['result' => false, 'Message' => $e->getMessage()];
            }
        }
        return $result;
    }

    /**
     * @param $result
     * @param $rmaData
     * @throws CouldNotSaveException
     */
    public function updateBizStatus($result, $rmaData)
    {
        foreach ($rmaData as $entity_id => $rma) {
            if ($result['code'] == '0000') {
                try {
                    $bizData = $this->bizDataRepository->getById($entity_id);
                    $bizData->setData('status', "01");
                    $bizData->setData('message', 'SUCCESS');
                    $this->bizDataRepository->save($bizData);
                } catch (Exception $exception) {
                    throw new CouldNotSaveException(
                        __('Could not update status : %1', $exception->getMessage()),
                        $exception
                    );
                }
            } else {
                $bizData = $this->bizDataRepository->getById($entity_id);
                $bizData->setData('status', "02");
                $bizData->setData('message', 'FAIL');
                $this->bizDataRepository->save($bizData);
            }
        }
    }
}