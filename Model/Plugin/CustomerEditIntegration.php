<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-03
 * Time: 오후 1:32
 */

namespace Eguana\BizConnect\Model\Plugin;

use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Eguana\BizConnect\Model\Customer\SendEdit;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

class CustomerEditIntegration
{
    const CUSTOMER_UPDATE_ENABLE= 'bizconnect/customer/update';

    /**
     * @var Session
     */
    private $session;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var SendEdit
     */
    private $sendEdit;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var RedirectFactory
     */
    private $resultRedirectFactory;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        Session $session,
        CustomerRepositoryInterface $customerRepository,
        RequestInterface $request,
        SendEdit $sendEdit,
        ManagerInterface $messageManager,
        UrlInterface $url,
        RedirectFactory $redirectFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->session = $session;
        $this->customerRepository = $customerRepository;
        $this->request = $request;
        $this->sendEdit = $sendEdit;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $redirectFactory;
        $this->url = $url;
        $this->scopeConfig = $scopeConfig;
    }

    public function aroundExecute(
        \Magento\Customer\Controller\Account\EditPost $subject,
        \Closure $proceed
    ) {
        if ($this->scopeConfig->getValue(self::CUSTOMER_UPDATE_ENABLE, 'store')) {
            $currentCustomer = $this->customerRepository->getById($this->session->getId());
            $current_email = $currentCustomer->getEmail();
            $current_firstname = $currentCustomer->getFirstname();
            $current_lastname = $currentCustomer->getLastname();

            $customerEmail = ($this->request->getParam('email')) ? $this->request->getParam('email') : $current_email;
            $password = ($this->request->getParam('password')) ? $this->request->getParam('password') : '';
            $first_name = ($this->request->getParam('firstname')) ? $this->request->getParam('firstname') : $current_firstname;
            $last_name = ($this->request->getParam('lastname')) ? $this->request->getParam('lastname') : $current_lastname;

            $editData = [
                'email' => $customerEmail,
                'password' => $password,
                'first_name' => $first_name,
                'last_name' => $last_name
            ];

            $result = $this->sendEdit->sendEditData($editData);

            if ($result['code'] == '0000') {
                $this->request->setParams(['email' => $result['data']['email']]);
                $this->request->setParams(['firstname' => $result['data']['first_name']]);
                $this->request->setParams(['lastname' => $result['data']['last_name']]);
                //password will be updated customer input value not value from I/F response.

                return $proceed();
            } else {
                $this->messageManager->addErrorMessage(
                    $result['message']
                );
                $defaultUrl = $this->url->getUrl('*/*/edit', ['_secure' => true]);
                /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setUrl($defaultUrl);
            }
        } else {
            return $proceed();
        }
    }
}