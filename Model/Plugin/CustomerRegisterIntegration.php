<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-03
 * Time: 오전 9:55
 */

namespace Eguana\BizConnect\Model\Plugin;

use Magento\Framework\App\RequestInterface;
use Eguana\BizConnect\Model\Customer\SendRegister;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

class CustomerRegisterIntegration
{
    const CUSTOMER_REGISTER_ENABLE= 'bizconnect/customer/register';

    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var SendRegister
     */
    private $sendRegister;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var RedirectFactory
     */
    private $resultRedirectFactory;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        RequestInterface $request,
        SendRegister $sendRegister,
        ManagerInterface $messageManager,
        RedirectFactory $redirectFactory,
        UrlInterface $url,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->request = $request;
        $this->sendRegister = $sendRegister;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $redirectFactory;
        $this->url = $url;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @throws LocalizedException
     */
    public function aroundExecute(
        \Magento\Customer\Controller\Account\CreatePost $subject,
        \Closure $proceed
    )
    {
        if ($this->scopeConfig->getValue(self::CUSTOMER_REGISTER_ENABLE, 'store')) {
            $customerEmail = $this->request->getParam('email');
            $password = $this->request->getParam('password');
            $first_name = $this->request->getParam('firstname');
            $last_name = $this->request->getParam('lastname');

            $registerData = [
                'email' => $customerEmail,
                'password' => $password,
                'first_name' => $first_name,
                'last_name' => $last_name
            ];

            $result = $this->sendRegister->sendCustomerData($registerData);

            if ($result['code'] == '0000') {
                $this->request->setParams(['email' => $result['data']['email']]);
                $this->request->setParams(['firstname' => $result['data']['first_name']]);
                $this->request->setParams(['lastname' => $result['data']['last_name']]);

                return $proceed();
            } else {
                $this->messageManager->addErrorMessage(
                    $result['message']
                );
                $defaultUrl = $this->url->getUrl('*/*/create', ['_secure' => true]);
                /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setUrl($defaultUrl);
            }
        } else {
            return $proceed();
        }
    }
}