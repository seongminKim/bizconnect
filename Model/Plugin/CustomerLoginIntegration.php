<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-03
 * Time: 오후 3:46
 */

namespace Eguana\BizConnect\Model\Plugin;

use Magento\Framework\App\RequestInterface;
use Eguana\BizConnect\Model\Customer\SendLogin;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class CustomerLoginIntegration
{
    const CUSTOMER_LOGIN_ENABLE= 'bizconnect/customer/login';

    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var SendLogin
     */
    private $sendLogin;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var RedirectFactory
     */
    private $resultRedirectFactory;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var CustomerFactory
     */
    private $customerFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        RequestInterface $request,
        SendLogin $sendLogin,
        Session $session,
        ManagerInterface $messageManager,
        UrlInterface $url,
        RedirectFactory $redirectFactory,
        CustomerFactory $customerFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->request = $request;
        $this->sendLogin = $sendLogin;
        $this->session = $session;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $redirectFactory;
        $this->url = $url;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    public function aroundExecute(
        \Magento\Customer\Controller\Account\LoginPost $subject,
        \Closure $proceed
    ) {
        if ($this->scopeConfig->getValue(self::CUSTOMER_LOGIN_ENABLE, 'store')) {
            $result = $this->sendLogin->sendLoginData();
            if ($result['code'] == '0000') {
                $customer = $this->loadCustomer($result['data']['email']);
                $this->session->setCustomerAsLoggedIn($customer);

                return $proceed();
            } else {
                $this->messageManager->addErrorMessage(
                    $result['message']
                );
                $defaultUrl = $this->url->getUrl('*/*/login', ['_secure' => true]);
                /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setUrl($defaultUrl);
            }
        } else {
            return $proceed();
        }
    }

    private function loadCustomer($email)
    {
        $customerFactory = $this->customerFactory->create();
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $customerFactory->setWebsiteId($websiteId);
        $customer = $customerFactory->loadByEmail($email);

        return $customer;
    }
}