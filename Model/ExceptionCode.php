<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-29
 * Time: 오후 1:13
 */

namespace Eguana\BizConnect\Model;

class ExceptionCode
{
    const INVALID_PARAM = '400';

    const NOT_FOUND = '404';

    const NOT_SUPPORTED_REQUEST = '405';

    const INTERNAL_SERVER_ERROR = '500';

    const INVALID_ACCESS_TOKEN = '1000';

    const INVALID_REFRESH_TOKEN = '1001';

    const INVALID_AUTHORIZATION_CODE = '1002';

    const UNSUPPORTED_RESPONSE_TYPE = '1003';

    const UNSUPPORTED_GRANT_TYPE = '1004';

    const UNKNOWN_MEMBER = '1100';

    const SLEEP_MEMBER = '1101';

    const PASSWORD_FAIL = '1102';

    const TOO_MANY_PASSWORD_FAIL = '1103';

    const DUPLICATE_EMAIL = '1110';

    const UNAUTHORIZED_CLIENT = '1200';

    const UNKNOWN_CLIENT = '1201';

    public function exceptionResult($code)
    {
        switch ($code) {
            case self::INVALID_PARAM:
                return __('Invalid Parameters');
            case self::NOT_FOUND:
                return __('Not Found');
            case self::NOT_SUPPORTED_REQUEST:
                return __('Requested method is not supported');
            case self::INTERNAL_SERVER_ERROR:
                return __('Internal Server Error');
            case self::INVALID_ACCESS_TOKEN:
                return __('Invalid Access Token');
            case self::INVALID_REFRESH_TOKEN:
                return __('Invalid Refresh Token');
            case self::INVALID_AUTHORIZATION_CODE:
                return __('Invalid Authorization Code');
            case self::UNSUPPORTED_RESPONSE_TYPE:
                return __('Unsupported Response Type');
            case self::UNSUPPORTED_GRANT_TYPE:
                return __('Unsupported Grant Type');
            case self::UNKNOWN_MEMBER:
                return __('Unknown Member');
            case self::SLEEP_MEMBER:
                return __('Sleep Member');
            case self::PASSWORD_FAIL:
                return __('Password fail');
            case self::TOO_MANY_PASSWORD_FAIL:
                return __('Too many password fail');
            case self::DUPLICATE_EMAIL:
                return __('DUPLICATE_EMAIL');
            case self::UNAUTHORIZED_CLIENT:
                return __('Unauthorized Client');
            case self::UNKNOWN_CLIENT:
                return __('Unknown Client');
            default:
                return $code;
        }
    }
}