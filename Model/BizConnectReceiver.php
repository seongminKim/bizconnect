<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-05
 * Time: 오후 1:43
 */

namespace Eguana\BizConnect\Model;

use Magento\Framework\Exception\FileSystemException as FilesystemExceptionAlias;
use Magento\Framework\Serialize\Serializer\Json;

class BizConnectReceiver
{

    const INTERFACE_TYPE_PRODUCT = 'product';

    const INTERFACE_TYPE_STOCK = 'stock';

    /**
     * @var Json
     */
    private $json;

    public function __construct(
        Json $json
    ) {
        $this->json = $json;
    }

    /**
     * Eguana BizConnect Receiver
     * @return mixed
     */
    public function saveBizData()
    {
        $postData = $this->getPostData();

        $receiveData = $this->json->unserialize($postData);

        return $receiveData;
    }

}
