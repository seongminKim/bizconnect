<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-06
 * Time: 오전 10:37
 */

namespace Eguana\BizConnect\Model\Stock;

use Eguana\BizConnect\Api\BizConnectStockReceiveInterface;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Api\Data\BizDataInterfaceFactory;
use Eguana\BizConnect\Model\BizDataSave;
use Magento\Framework\Exception\FileSystemException as FilesystemExceptionAlias;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;

class StockBizDataSave extends BizDataSave implements BizConnectStockReceiveInterface
{
    /**
     * @var File
     */
    private $file;

    public function __construct(
        BizDataInterfaceFactory $bizDataInterfaceFactory,
        StoreManagerInterface $storeManager,
        Json $json,
        BizDataRepositoryInterface $bizDataRepository,
        File $file
    ) {
        parent::__construct($bizDataInterfaceFactory, $storeManager, $json, $bizDataRepository);
        $this->file = $file;
    }

    public function saveStockBizData()
    {
        $postData = $this->getPostData();

        $receiveData = $this->json->unserialize($postData);

        $type = $receiveData['type'];
        $items = $receiveData['items'];

        if ($type !== self::INTERFACE_TYPE_STOCK) {
            $message = ['code' => '400', 'message' => 'Wrong type with current endpoint'];
            return $this->json->serialize($message);
        }

        $message = $this->saveBizData($type, $items);

        return $message;
    }

    /**
     * get JSON Data from GoodMD Request
     */
    public function getPostData()
    {
        try {
            $requestData = $this->file->fileGetContents("php://input");
        } catch (FilesystemExceptionAlias $e) {
            $requestData = $e;
        }

        return $requestData;
    }
}