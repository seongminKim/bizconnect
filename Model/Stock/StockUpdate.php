<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-11
 * Time: 오전 9:18
 */

namespace Eguana\BizConnect\Model\Stock;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;

class StockUpdate
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var SourceItemInterface
     */
    private $sourceItem;
    /**
     * @var SourceItemsSaveInterface
     */
    private $sourceItemsSave;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        SourceItemInterface $sourceItem,
        SourceItemsSaveInterface $sourceItemsSave
    ) {

        $this->productRepository = $productRepository;
        $this->sourceItem = $sourceItem;
        $this->sourceItemsSave = $sourceItemsSave;
    }

    public function updateStockData($stockData)
    {
        $result = [];

        foreach ($stockData as $entity_id => $stock) {
            $sku = $stock['sku'];
            $qty = $stock['qty'];

            $product = $this->_getProduct($sku);

            if (!$product) {
                $result[$entity_id] = ['result' => false, 'message' => 'Product not exist'];
            } else {
                $update_result = $this->_updateProductQty($product, $qty);
                if ($update_result['result']) {
                    $result[$entity_id] = ['result' => true, 'message' => 'SUCCESS'];
                } else {
                    $result[$entity_id] = ['result' => false, 'message' => $update_result['message']];
                }
            }
        }

        return $result;
    }

    private function _getProduct($sku)
    {
        try {
            $product = $this->productRepository->get($sku, false);
        } catch (NoSuchEntityException $e) {
            return false;
        }

        return $product;
    }

    private function _updateProductQty($product, $qty)
    {
        try {
            $this->sourceItem->setSku($product->getSku());
            $this->sourceItem->setQuantity($qty);
            $this->sourceItem->setStatus(1);
            if ($qty == 0) {
                $this->sourceItem->setStatus(0);
            }
            $this->sourceItem->setSourceCode('default');
            $this->sourceItemsSave->execute([$this->sourceItem]);

            $update_result = ['result' => true, 'message' =>'SUCCESS'];
        } catch (\Exception $e) {
            $update_result = ['result' => false, 'message' =>$e->getMessage()];
        }

        return $update_result;
    }
}