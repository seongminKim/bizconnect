<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 1:48
 */

namespace Eguana\BizConnect\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class BizData extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('eguana_bizconnect', 'entity_id');
    }
}