<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 1:53
 */

namespace Eguana\BizConnect\Model\ResourceModel\BizData;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Eguana\BizConnect\Model\BizData;
use Eguana\BizConnect\Model\ResourceModel\BizData as ResourceModelBizData;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    public $_idFieldName = 'entity_id';

    /**
     * Resource initialization
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(BizData::class, ResourceModelBizData::class);
    }
}