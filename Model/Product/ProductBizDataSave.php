<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-06
 * Time: 오전 10:32
 */

namespace Eguana\BizConnect\Model\Product;

use Eguana\BizConnect\Api\BizConnectProductReceiveInterface;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Api\Data\BizDataInterfaceFactory;
use Eguana\BizConnect\Model\BizDataSave;
use Magento\Framework\Exception\FileSystemException as FilesystemExceptionAlias;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;

class ProductBizDataSave extends BizDataSave implements BizConnectProductReceiveInterface
{
    /**
     * @var File
     */
    private $file;

    public function __construct(
        BizDataInterfaceFactory $bizDataInterfaceFactory,
        StoreManagerInterface $storeManager,
        Json $json,
        BizDataRepositoryInterface $bizDataRepository,
        File $file
    ) {
        parent::__construct($bizDataInterfaceFactory, $storeManager, $json, $bizDataRepository);
        $this->file = $file;
    }

    /**
     * @param $type
     * @param $items
     * @return array
     */
    public function saveProductBizData()
    {
        $postData = $this->getPostData();

        $receiveData = $this->json->unserialize($postData);

        $type = $receiveData['type'];
        $items = $receiveData['items'];

        if ($type !== self::INTERFACE_TYPE_PRODUCT) {
            $message = ['code' => '400', 'message' => 'Wrong type with current endpoint'];
            return $this->json->serialize($message);
        }

        $message = $this->saveBizData($type, $items);

        return $message;
    }

    /**
     * get JSON Data from GoodMD Request
     */
    public function getPostData()
    {
        try {
            $requestData = $this->file->fileGetContents("php://input");
        } catch (FilesystemExceptionAlias $e) {
            $requestData = $e;
        }

        return $requestData;
    }
}