<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-02
 * Time: 오전 9:03
 */

namespace Eguana\BizConnect\Model\Product;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Catalog\Api\ProductRepositoryInterface;

class ProductCreate
{

    /**
     * @var ProductFactory
     */
    private $productFactory;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(
        ProductFactory $productFactory,
        Json $json,
        ProductRepositoryInterface $productRepository
    ) {
        $this->productFactory = $productFactory;
        $this->json = $json;
        $this->productRepository = $productRepository;
    }

    public function productIntegration($productData)
    {
        $result = [];

        foreach ($productData as $entity_id => $productValue) {
            $sku = $productValue['sku'];
            $name = $productValue['name'];
            $price = $productValue['price'];

            $product = $this->_getProduct($sku);

            if (!$product) {
                $product = $this->_createProduct($sku, $name, $price);
            } else {
                $product = $this->_updateProductData($product, $name, $price);
            }

            $result[$entity_id] = $this->_saveProduct($product);
        }

        return $result;
    }

    private function _getProduct($sku)
    {
        try {
            $product = $this->productRepository->get($sku, false);
        } catch (NoSuchEntityException $e) {
            return false;
        }

        return $product;
    }

    private function _createProduct($sku, $name, $price)
    {
        /** @var $product Product */
        $product = $this->productFactory->create();

        $product->setSku($sku);
        $product->setTypeId('simple');
        $product->setData('name', $name);
        $product->setData('price', $price);
        $product->setStatus(2);
        $product->setData('visibility', 4);
        $product->setAttributeSetId(4);

        return $product;
    }

    private function _updateProductData($product, $name, $price)
    {
        $product->setData('name', $name);
        $product->setData('price', $price);

        return $product;
    }

    private function _saveProduct($product)
    {
        try {
            $this->productRepository->save($product);
            $result = ['result' => true, 'message' => 'SUCCESS'];
        } catch (Exception $e) {
            $result = ['result' => false, 'message' => $e->getMessage()];
        }

        return $result;
    }
}