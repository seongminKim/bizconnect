<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-15
 * Time: 오전 10:59
 */

namespace Eguana\BizConnect\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Status implements OptionSourceInterface
{
    const BIZ_CONNECT_PENDING = '00';

    const BIZ_CONNECT_SUCCESS = '01';

    const BIZ_CONNECT_FAIL = '02';

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::BIZ_CONNECT_PENDING, 'label'=> 'Pending'],
            ['value' => self::BIZ_CONNECT_SUCCESS, 'label'=> 'Success'],
            ['value' => self::BIZ_CONNECT_FAIL, 'label'=> 'Fail']
        ];
    }
}