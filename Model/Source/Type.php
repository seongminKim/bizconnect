<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-10
 * Time: 오후 2:31
 */

namespace Eguana\BizConnect\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Type implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'order', 'label' =>'Order'],
            ['value' => 'rma', 'label' =>'RMA'],
            ['value' => 'product', 'label' =>'Product'],
            ['value' => 'stock', 'label' =>'Stock']
        ];
    }
}