<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-12
 * Time: 오후 2:01
 */

namespace Eguana\BizConnect\Model;

use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Api\Data\BizDataInterface;
use Magento\Cms\Api\Data\BlockSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Eguana\BizConnect\Model\ResourceModel\BizData as ResourceBizData;
use Eguana\BizConnect\Model\BizDataFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Eguana\BizConnect\Model\ResourceModel\BizData\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Eguana\BizConnect\Api\Data\BizDataSearchResultInterfaceFactory;

class BizDataRepository implements BizDataRepositoryInterface
{

    /**
     * @var Resource
     */
    private $resource;
    /**
     * @var \Eguana\BizConnect\Model\BizDataFactory
     */
    private $bizDataFactory;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var BizDataSearchResultInterfaceFactory
     */
    private $searchResultInterfaceFactory;

    private $instance = [];

    public function __construct(
        ResourceBizData $resource,
        BizDataFactory $bizDataFactory,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        BizDataSearchResultInterfaceFactory $searchResultInterfaceFactory
    ) {
        $this->resource = $resource;
        $this->bizDataFactory = $bizDataFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultInterfaceFactory = $searchResultInterfaceFactory;
    }

    /**
     * @param BizDataInterface $bizData
     * @return BizDataInterface|mixed
     * @throws CouldNotSaveException
     */
    public function save(BizDataInterface $bizData)
    {
        try {
            $this->resource->save($bizData);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $bizData;
    }

    /**
     * @param $entity_id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($entity_id)
    {
        $bizDataFactory = $this->bizDataFactory->create();
        $this->resource->load($bizDataFactory, $entity_id);
        if (!$bizDataFactory->getId()) {
            throw new NoSuchEntityException(__('Biz Data with id "%1" does not exist.', $entity_id));
        }
        return $bizDataFactory;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Eguana\BizConnect\Api\Data\BizDataSearchResultInterface|BlockSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Eguana\BizConnect\Model\ResourceModel\BizData\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var \Eguana\BizConnect\Api\Data\BizDataSearchResultInterface $searchResults */
        $searchResults = $this->searchResultInterfaceFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param BizDataInterface $bizData
     * @return bool|mixed
     * @throws CouldNotDeleteException
     */
    public function delete(BizDataInterface $bizData)
    {
        try {
            $entityId = $bizData->getEntityId();
            $this->resource->delete($bizData);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        unset($this->instance[$entityId]);
        return true;
    }

    /**
     * @param $entity_id
     * @return bool|mixed
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($entity_id)
    {
        return $this->delete($this->getById($entity_id));
    }
}
