<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-09
 * Time: 오전 9:20
 */

namespace Eguana\BizConnect\Model\Authorize;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\ExceptionCode;
use Magento\Customer\Model\Session;

class Token extends ExceptionCode
{
    const AUTHORIZE_TOKEN_URL = 'https://commerce.apdigit.com/v1.0/authorize/token';
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var Session
     */
    private $session;

    public function __construct(
        Curl $curl,
        Json $json,
        Session $session
    ) {
        $this->curl = $curl;
        $this->json = $json;
        $this->session = $session;
    }

    public function callTokenApi()
    {
        $jwt = $this->createJWT();
//        $authorize_params = [
//            'grant_type' => 'password',
//            'client_id' => 'bdfbd',
//            'client_secret' => 'qdqdwd',
//            'account' => $jwt
//        ];

//        $jsonPayload = $this->json->serialize($authorize_params);

//        $this->curl->post(self::AUTHORIZE_TOKEN_URL, $jsonPayload);

//        $response = $this->curl->getBody();

        $response = '{
        "code" : "0000",
"message" : "SUCCESS",
"data" : {
            "access_token": "fjwO2Z9reRMPpr9-JsiiDXKrrl0PFSb_b9CDh1122",
"refresh_token": "6bhJF7yfNkzO3IZH-dkP5_74OKZ6CBKxVT_f42UQ8cw",
"token_type": "Bearer",
"expire_in": 3600
}
}';

        $result = $this->json->unserialize($response);

        if ($result['code'] == '0000') {
            $this->setTokenToCustomerSession($result['data']['access_token']);

            if (!$this->session->getAuthorizeToken()) {
                $result = ['code' => '401', 'message' => 'Invalid Access Token'];
            }
            $result_message = $result;
        } else {
            $exception_message = $this->exceptionResult($result['code']);
            $result_message = ['code' => $result['code'], 'message' => $exception_message];
        }

        return $result_message;
    }

    private function setTokenToCustomerSession($access_token)
    {
        $this->session->setAuthorizeToken($access_token);
    }

    private function createJWT()
    {
        $header = json_encode(['type' => 'JWT', 'alg' => 'HS256']);
        $payload = json_encode(['user_id' => 'test', 'password' => 'test']); //need value
        $secret = 'bascasdqwe12cas'; // need value

        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);

        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }
}