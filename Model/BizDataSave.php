<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-06
 * Time: 오전 11:23
 */

namespace Eguana\BizConnect\Model;

use Eguana\BizConnect\Api\Data\BizDataInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;

class BizDataSave
{
    const INTERFACE_TYPE_PRODUCT = 'product';

    const INTERFACE_TYPE_STOCK = 'stock';
    /**
     * @var BizDataInterfaceFactory
     */
    private $bizDataInterfaceFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Json
     */
    protected $json;
    /**
     * @var BizDataRepositoryInterface
     */
    private $bizDataRepository;

    public function __construct(
        BizDataInterfaceFactory $bizDataInterfaceFactory,
        StoreManagerInterface $storeManager,
        Json $json,
        BizDataRepositoryInterface $bizDataRepository
    ) {
        $this->bizDataInterfaceFactory = $bizDataInterfaceFactory;
        $this->storeManager = $storeManager;
        $this->json = $json;
        $this->bizDataRepository = $bizDataRepository;
    }

    /**
     * Eguana BizConnect data saver
     * @param $type
     * @param $items
     * @return mixed
     */
    public function saveBizData($type, $items)
    {
        if (isset($items)) {
            $message =[];

            foreach ($items as $key => $item) {
                $bizDataFactory = $this->bizDataInterfaceFactory->create();
                try {
                    $bizDataFactory->setType($type);
                    $bizDataFactory->setWebsiteId($this->storeManager->getStore()->getWebsiteId());
                    $bizDataFactory->setBizData($this->json->serialize($item));
                    $this->bizDataRepository->save($bizDataFactory);

                    $message = ['code' => '200', 'message' => 'success'];
                } catch (\Exception $e) {
                    $message = ['code' => $e->getCode(), 'message' => $e->getMessage()];
                    continue;
                }
            }
            return $this->json->serialize($message);
        }
    }
}