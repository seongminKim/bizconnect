<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-15
 * Time: 오전 10:42
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Eguana\BizConnect\Controller\Adminhtml\AbstractAction;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Index extends AbstractAction
{

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage
            ->setActiveMenu('Eguana_BizConnect::BizConnect')
            ->getConfig()->getTitle()->prepend(__('BizConnect'));

        return $resultPage;
    }
}