<?php
/**
 * Created by PhpStorm.
 * User: glenn
 * Date: 2019-06-13
 * Time: 오후 5:08
 */
namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Eguana\BizConnect\Controller\Adminhtml\AbstractAction;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Magento\Backend\App\Action\Context;

/**
 * Class Delete
 * @package Eguana\BizConnect\Controller\Adminhtml\BizData
 */
class Delete extends AbstractAction
{
    /**
     * @var BizDataRepositoryInterface
     */
    private $bizDataRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param BizDataRepositoryInterface $bizDataRepository
     */
    public function __construct(
        Context $context,
        BizDataRepositoryInterface $bizDataRepository
    ) {
        parent::__construct($context);
        $this->bizDataRepository = $bizDataRepository;
    }

    /**
     * Delete banner action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int)$this->getRequest()->getParam('entity_id');
        if ($id) {
            try {
                $this->bizDataRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('row was successfully deleted'));
                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        $this->messageManager->addErrorMessage(__('row could not be deleted'));
        return $resultRedirect->setPath('*/*/index');
    }
}
