<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:10
 */
namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Eguana\BizConnect\Controller\Adminhtml\AbstractAction;
use Eguana\BizConnect\Model\BizData;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;

class Edit extends AbstractAction
{
    public $coreRegistry;
    /**
     * @var BizData
     */
    private $bizData;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param BizData $bizData
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        BizData $bizData
    ) {
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context);
        $this->bizData = $bizData;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->bizData;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This log no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->coreRegistry->register('bizconnect_bizdata', $model);

        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit BizConnect') : __('New BizConnect'),
            $id ? __('Edit BizConnect') : __('New BizConnect')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('BizConnect'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New BizConnect'));
        return $resultPage;
    }
}
