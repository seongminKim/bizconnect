<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-10
 * Time: 오전 9:35
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Ui\Component\MassAction\Filter;
use Eguana\BizConnect\Model\ResourceModel\BizData\CollectionFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\Order\OrderDataIntegration;
use Eguana\BizConnect\Model\Order\Send;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\Model\View\Result\Redirect;

class MassSend extends Action
{

    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var OrderDataIntegration
     */
    private $orderDataIntegration;
    /**
     * @var Send
     */
    private $orderSend;

    public function __construct(
        Action\Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        Json $json,
        OrderDataIntegration $orderDataIntegration,
        Send $orderSend
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->json = $json;
        $this->orderDataIntegration = $orderDataIntegration;
        $this->orderSend = $orderSend;
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $orderData = [];

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        foreach ($collection as $data) {
            if ($data->getData('type') == 'order') {
                if ($data->getData('status') !== '01') {
                    $orderData[$data->getEntityId()] = $this->json->unserialize($data->getBizData());
                }
            } else {
                $this->messageManager->addErrorMessage(__('Please Select Order Data only.'));
                return $resultRedirect->setPath('*/*/index');
            }
        }

        $orderCount = count($orderData);

        if ($orderCount > 0) {
            $requestData = $this->json->serialize($orderData);
            $result = $this->orderSend->sendData($requestData);

            if ($result['code'] == '0000') {
                $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been sent.', $orderCount));
            } else {
                $this->messageManager->addErrorMessage($result['message']);
            }

            try {
                $this->orderDataIntegration->updateBizStatus($result, $orderData);
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        } else {
            $this->messageManager->addErrorMessage('There is no data to send');
        }

        return $resultRedirect->setPath('*/*/index');
    }
}