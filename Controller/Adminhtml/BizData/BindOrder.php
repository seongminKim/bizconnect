<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-10
 * Time: 오전 11:12
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Magento\Backend\App\Action;
use Eguana\BizConnect\Model\Order\OrderDataIntegration;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\Model\View\Result\Redirect;


class BindOrder extends Action
{

    /**
     * @var OrderDataIntegration
     */
    private $orderDataIntegration;

    public function __construct(
        Action\Context $context,
        OrderDataIntegration $orderDataIntegration,
        ResultFactory $resultFactory
    ) {
        parent::__construct($context);
        $this->orderDataIntegration = $orderDataIntegration;
        $this->resultFactory = $resultFactory;
    }

    public function execute()
    {
        $bindData = $this->orderDataIntegration->bindOrderData();
        $result = $this->orderDataIntegration->saveBizData($bindData);
        $this->orderDataIntegration->updateOrderItemStatus($bindData);

        $resultSize = count($result);
        if ($resultSize > 0) {
            $this->messageManager->addSuccessMessage(__('%1Data(s) binding Success.', $resultSize));
        } else {
            $this->messageManager->addErrorMessage(__('No data to bind'));
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}