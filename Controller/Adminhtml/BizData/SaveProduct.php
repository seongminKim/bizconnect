<?php
/**
 * Created by Eguana.
 * User: Brian
 * Date: 2019-12-11
 * Time: 오후 3:20
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\Model\View\Result\Redirect;
use Eguana\BizConnect\Model\Product\ProductCreate;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Ui\Component\MassAction\Filter;
use Eguana\BizConnect\Model\ResourceModel\BizData\CollectionFactory;

class SaveProduct extends Action
{
    /**
     * @var ProductCreate
     */
    private $productCreate;
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var BizDataRepositoryInterface
     */
    private $bizDataRepository;

    /**
     * SaveProduct constructor.
     * @param Action\Context $context
     * @param Filter $filter
     * @param ProductCreate $productCreate
     * @param ResultFactory $resultFactory
     * @param Json $json
     * @param BizDataRepositoryInterface $bizDataRepository
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Action\Context $context,
        BizDataRepositoryInterface $bizDataRepository,
        Filter $filter,
        ProductCreate $productCreate,
        ResultFactory $resultFactory,
        Json $json,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($context);
        $this->productCreate = $productCreate;
        $this->resultFactory = $resultFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->json = $json;
        $this->bizDataRepository = $bizDataRepository;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     * @throws CouldNotSaveException
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/saveproducts_%s.log',date('Ymd')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);

        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $productData = [];

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        foreach ($collection as $data) {
            if ($data->getData('type') == 'product') {
                if ($data->getData('status') !== '01') {
                    $productData[$data->getEntityId()] = $this->json->unserialize($data->getBizData());
                }
            } else {
                $this->messageManager->addErrorMessage(__('Please Select Products Data only.'));
                return $resultRedirect->setPath('*/*/index');
            }
        }

        $productCount = count($productData);

        $logger->info($productCount);
        if ($productCount > 0) {
            $result = $this->productCreate->productIntegration($productData);

            try {
                $this->updateBizStatus($result);
                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been created.', $productCount)
                );
            } catch (CouldNotSaveException $exception) {
                throw new CouldNotSaveException(
                    __('Could not update customer status : %1', $exception->getMessage()),
                    $exception
                );
            }
        } else {
            $this->messageManager->addErrorMessage('There is no product to create');
        }

        return $resultRedirect->setPath('*/*/');
    }


    /**
     * @param $result
     * @throws CouldNotSaveException
     */
    public function updateBizStatus($result)
    {
        foreach ($result as $entity_id => $resultData) {
            if ($resultData['result']) {
                try {
                    $bizData = $this->bizDataRepository->getById($entity_id);
                    $bizData->setData('status', "01");
                    $bizData->setData('message', $resultData['message']);
                    $this->bizDataRepository->save($bizData);
                } catch (Exception $exception) {
                    throw new CouldNotSaveException(
                        __('Could not update status : %1', $exception->getMessage()),
                        $exception
                    );
                }
            } else {
                $bizData = $this->bizDataRepository->getById($entity_id);
                $bizData->setData('status', "02");
                $bizData->setData('message', $resultData['message']);
                $this->bizDataRepository->save($bizData);
            }
        }
    }
}