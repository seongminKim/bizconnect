<?php
/**
 * Created by Eguana.
 * User: Brian
 * Date: 2019-12-12
 * Time: 오후 3:05
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Eguana\BizConnect\Model\Rma\Send;
use Eguana\BizConnect\Model\Rma\RmaDataIntegration;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Ui\Component\MassAction\Filter;
use Eguana\BizConnect\Model\ResourceModel\BizData\CollectionFactory;

class MassSendReturn extends Action
{
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var RmaDataIntegration
     */
    private $rmaDataIntegration;
    /**
     * @var Send
     */
    private $rmaSend;

    /**
     * MassSendReturn constructor.
     * @param Action\Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param Json $json
     * @param RmaDataIntegration $rmaDataIntegration
     * @param Send $rmaSend
     */
    public function __construct(
        Action\Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        Json $json,
        RmaDataIntegration $rmaDataIntegration,
        Send $rmaSend
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->json = $json;
        $this->rmaDataIntegration = $rmaDataIntegration;
        $this->rmaSend = $rmaSend;
    }
    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $rmaData = [];

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        foreach ($collection as $data) {
            if ($data->getData('type') == 'rma') {
                if ($data->getData('status') !== '01') {
                    $orderData[$data->getEntityId()] = $this->json->unserialize($data->getBizData());
                }
            } else {
                $this->messageManager->addErrorMessage(__('Please Select Return Data only.'));
                return $resultRedirect->setPath('*/*/index');
            }
        }

        $rmaCount = count($rmaData);

        if ($rmaCount > 0) {
            $requestData = $this->json->serialize($rmaData);
            $result = $this->rmaSend->sendData($requestData);

            if ($result['code'] == '0000') {
                $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been sent.', $rmaCount));
            } else {
                $this->messageManager->addErrorMessage($result['message']);
            }

            try {
                $this->rmaDataIntegration->updateBizStatus($result, $rmaData);
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        } else {
            $this->messageManager->addErrorMessage('There is no data to send');
        }
        return $resultRedirect->setPath('*/*/index');
    }
}