<?php
/**
 * Created by Eguana.
 * User: Brian
 * Date: 2019-12-12
 * Time: 오전 11:12
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Eguana\BizConnect\Model\Rma\RmaDataIntegration;

class BindReturn extends Action
{
    /**
     * @var RmaDataIntegration
     */
    private $rmaDataIntegration;

    public function __construct(
        Action\Context $context,
        ResultFactory $resultFactory,
        RmaDataIntegration $rmaDataIntegration
    ) {
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
        $this->rmaDataIntegration = $rmaDataIntegration;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $bindData = $this->rmaDataIntegration->bindRmaData();
        $result = $this->rmaDataIntegration->saveBizData($bindData);
        $this->rmaDataIntegration->updateRmaItemStatus($bindData);

        $resultSize = count($result);
        if ($resultSize > 0) {
            $this->messageManager->addSuccessMessage(__('%1Data(s) binding Success.', $resultSize));
        } else {
            $this->messageManager->addErrorMessage(__('No data to bind'));
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}