<?php
/**
 * Created by Eguana.
 * User: Brian
 * Date: 2019-12-12
 * Time: 오전 9:34
 */

namespace Eguana\BizConnect\Controller\Adminhtml\BizData;


use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\ResourceModel\BizData\CollectionFactory;
use Eguana\BizConnect\Model\Stock\StockUpdate;

class UpdateStockQty extends Action
{
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var StockUpdate
     */
    private $stockUpdate;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var BizDataRepositoryInterface
     */
    private $bizDataRepository;

    public function __construct(
        Action\Context $context,
        BizDataRepositoryInterface $bizDataRepository,
        Filter $filter,
        StockUpdate $stockUpdate,
        Json $json,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->stockUpdate = $stockUpdate;
        $this->collectionFactory = $collectionFactory;
        $this->json = $json;
        $this->bizDataRepository = $bizDataRepository;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/update_stock_%s.log',date('Ymd')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);

        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $stockData = [];

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        foreach ($collection as $data) {
            if ($data->getData('type') == 'stock') {
                if ($data->getData('status') !== '01') {
                    $stockData[$data->getEntityId()] = $this->json->unserialize($data->getBizData());
                }
            } else {
                $this->messageManager->addErrorMessage(__('Please Select Stock Data only.'));
                return $resultRedirect->setPath('*/*/index');
            }
        }

        $stockCount = count($stockData);

        $logger->info($stockCount);

        if ($stockCount > 0) {
            $result = $this->stockUpdate->updateStockData($stockData);

            try {
                $this->updateBizStatus($result);
                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been created.', $stockCount)
                );
            } catch (CouldNotSaveException $exception) {
                throw new CouldNotSaveException(
                    __('Could not update customer status : %1', $exception->getMessage()),
                    $exception
                );
            }
        } else {
            $this->messageManager->addErrorMessage('There is no stock to update qty');
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param $result
     * @throws CouldNotSaveException
     */
    public function updateBizStatus($result)
    {
        foreach ($result as $entity_id => $resultData) {
            if ($resultData['result']) {
                try {
                    $bizData = $this->bizDataRepository->getById($entity_id);
                    $bizData->setData('status', "01");
                    $bizData->setData('message', $resultData['message']);
                    $this->bizDataRepository->save($bizData);
                } catch (Exception $exception) {
                    throw new CouldNotSaveException(
                        __('Could not update status : %1', $exception->getMessage()),
                        $exception
                    );
                }
            } else {
                $bizData = $this->bizDataRepository->getById($entity_id);
                $bizData->setData('status', "02");
                $bizData->setData('message', $resultData['message']);
                $this->bizDataRepository->save($bizData);
            }
        }
    }
}