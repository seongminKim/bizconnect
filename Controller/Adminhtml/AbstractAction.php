<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:08
 */

namespace Eguana\BizConnect\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class AbstractAction extends Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Eguana_BizConnect::bizConnect';

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Eguana_BizConnect::bizConnect')
            ->addBreadcrumb(__('BizConnect'), __('BizConnect'));
        return $resultPage;
    }
}