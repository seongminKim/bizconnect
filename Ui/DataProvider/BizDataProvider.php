<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-15
 * Time: 오후 2:36
 */

namespace Eguana\BizConnect\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Eguana\BizConnect\Model\ResourceModel\BizData\Grid\CollectionFactory;
use Magento\Framework\App\RequestInterface;

class BizDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    private $_coreRegistry;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        \Magento\Framework\Registry $coreRegistry,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = [];
        $bizModel = $this->_coreRegistry->registry('bizconnect_bizdata');

        if (!empty($bizModel)) {
            $data[$bizModel->getId()] = $bizModel->getData();
        } else {
            $id = $this->request->getParam($this->getRequestFieldName());
            foreach ($this->getCollection()->getItems() as $bizData) {
                if ($id == $bizData->getId()) {
                    $data[$id] = $bizData->getData();
                }
            }
        }
        return $data;
    }
}