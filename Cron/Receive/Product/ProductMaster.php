<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-20
 * Time: 오전 9:35
 */

namespace Eguana\BizConnect\Cron\Receive\Product;

use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Cron\Receive\AbstractReceiver;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Eguana\BizConnect\Model\Product\ProductCreate;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;

class ProductMaster extends AbstractReceiver
{
    /**
     * @var ProductCreate
     */
    private $productCreate;
    /**
     * @var Json
     */
    private $json;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        BizDataRepositoryInterface $bizDataRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductCreate $productCreate,
        Json $json
    ) {
        parent::__construct($scopeConfig, $bizDataRepository, $searchCriteriaBuilder);
        $this->productCreate = $productCreate;
        $this->json = $json;
    }

    /**
     * @throws CouldNotSaveException
     */
    public function execute()
    {
        if ($this->productInterfaceIsEnable()) {
            $productData = $this->getProductBizData();
            $result = $this->productCreate->productIntegration($productData);

            try {
                $this->updateBizStatus($result);
            } catch (CouldNotSaveException $exception) {
                throw new CouldNotSaveException(
                    __('Could not update customer status : %1', $exception->getMessage()),
                    $exception
                );
            }
        }
    }

    private function getProductBizData()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', '00')
            ->addFilter('type', 'product')
            ->create();

        $productBizData = $this->bizDataRepository->getList($searchCriteria);

        $productData = [];

        foreach ($productBizData->getItems() as $product) {
            $productData[$product['entity_id']] = $this->json->unserialize($product['biz_data']);
        }

        return $productData;
    }
}