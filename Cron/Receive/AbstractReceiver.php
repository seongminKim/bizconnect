<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-11-20
 * Time: 오전 9:40
 */

namespace Eguana\BizConnect\Cron\Receive;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\SearchCriteriaBuilder;

class AbstractReceiver
{
    const PRODUCT_ENABLE_PATH = 'bizconnect/product/active';

    const STOCK_ENABLE_PATH = 'bizconnect/stock/active';

    /**
     * @var ScopeConfigInterface
     */
    public $scopeConfig;
    /**
     * @var BizDataRepositoryInterface
     */
    public $bizDataRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    public $searchCriteriaBuilder;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        BizDataRepositoryInterface $bizDataRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->bizDataRepository = $bizDataRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return mixed
     */
    public function productInterfaceIsEnable()
    {
        return $this->scopeConfig->getValue(self::PRODUCT_ENABLE_PATH, 'store');
    }

    /**
     * @return mixed
     */
    public function stockInterfaceIsEnable()
    {
        return $this->scopeConfig->getValue(self::STOCK_ENABLE_PATH, 'store');
    }

    /**
     * @param $result
     * @throws CouldNotSaveException
     */
    public function updateBizStatus($result)
    {
        foreach ($result as $entity_id => $resultData) {
            if ($resultData['result']) {
                try {
                    $bizData = $this->bizDataRepository->getById($entity_id);
                    $bizData->setData('status', "01");
                    $bizData->setData('message', $resultData['message']);
                    $this->bizDataRepository->save($bizData);
                } catch (Exception $exception) {
                    throw new CouldNotSaveException(
                        __('Could not update status : %1', $exception->getMessage()),
                        $exception
                    );
                }
            } else {
                $bizData = $this->bizDataRepository->getById($entity_id);
                $bizData->setData('status', "02");
                $bizData->setData('message', $resultData['message']);
                $this->bizDataRepository->save($bizData);
            }
        }
    }
}