<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-11
 * Time: 오전 9:13
 */

namespace Eguana\BizConnect\Cron\Receive\Stock;

use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Cron\Receive\AbstractReceiver;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Eguana\BizConnect\Model\Stock\StockUpdate;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;


class Stock extends AbstractReceiver
{
    /**
     * @var Json
     */
    private $json;
    /**
     * @var StockUpdate
     */
    private $stockUpdate;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        BizDataRepositoryInterface $bizDataRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Json $json,
        StockUpdate $stockUpdate
    ) {
        parent::__construct($scopeConfig, $bizDataRepository, $searchCriteriaBuilder);
        $this->json = $json;
        $this->stockUpdate = $stockUpdate;
    }

    /**
     * @throws CouldNotSaveException
     */
    public function execute()
    {
        if ($this->stockInterfaceIsEnable()) {
            $stockData = $this->getStockBizData();
            $result = $this->stockUpdate->updateStockData($stockData);

            try {
                $this->updateBizStatus($result);
            } catch (CouldNotSaveException $exception) {
                throw new CouldNotSaveException(
                    __('Could not update customer status : %1', $exception->getMessage()),
                    $exception
                );
            }
        }
    }

    private function getStockBizData()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', '00')
            ->addFilter('type', 'stock')
            ->create();

        $stockBizData = $this->bizDataRepository->getList($searchCriteria);

        $stockData = [];

        foreach ($stockBizData->getItems() as $stock) {
            $stockData[$stock['entity_id']] = $this->json->unserialize($stock['biz_data']);
        }

        return $stockData;
    }
}