<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-02
 * Time: 오후 1:00
 */

namespace Eguana\BizConnect\Cron\Send;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;

class AbstractSender
{
    const XML_ORDER_PATH_ACTIVE  = 'bizconnect/order/active';

    const XML_RMA_PATH_ACTIVE  = 'bizconnect/rma/active';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var BizDataRepositoryInterface
     */
    public $bizDataRepository;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        BizDataRepositoryInterface $bizDataRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->bizDataRepository = $bizDataRepository;
    }

    public function orderInterfaceIsEnable()
    {
        return $this->scopeConfig->getValue(self::XML_ORDER_PATH_ACTIVE, 'store');
    }

    public function rmaInterfaceIsEnable()
    {
        return $this->scopeConfig->getValue(self::XML_RMA_PATH_ACTIVE, 'store');
    }
}
