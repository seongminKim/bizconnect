<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-02
 * Time: 오후 12:59
 */

namespace Eguana\BizConnect\Cron\Send\Order;

use Eguana\BizConnect\Model\Order\Send;
use Eguana\BizConnect\Cron\Send\AbstractSender;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\Order\OrderDataIntegration;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;

class OrderSend extends AbstractSender
{
    /**
     * @var OrderDataIntegration
     */
    private $orderDataIntegration;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var Send
     */
    private $send;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        OrderDataIntegration $orderDataIntegration,
        Json $json,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        BizDataRepositoryInterface $bizDataRepository,
        Send $send
    ) {
        parent::__construct($scopeConfig, $bizDataRepository);
        $this->orderDataIntegration = $orderDataIntegration;
        $this->json = $json;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->send = $send;
    }

    public function execute()
    {
        if ($this->orderInterfaceIsEnable()) {
            $orderData = $this->getOrderBizData();
            $requestData = $this->json->serialize($orderData);

            $result = $this->send->sendData($requestData);

            try {
                $this->orderDataIntegration->updateBizStatus($result, $orderData);
                $message = 'SUCCESS';
            } catch (CouldNotSaveException $e) {
                $message = $e->getMessage();
            }
            return $message;
        }
    }

    private function getOrderBizData()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', '00')
            ->addFilter('type', 'Order')
            ->create();

        $orderBizData = $this->bizDataRepository->getList($searchCriteria);
        $orderData = [];

        foreach ($orderBizData->getItems() as $order) {
            $orderData[$order['entity_id']] = $this->json->unserialize($order['biz_data']);
        }

        return $orderData;
    }
}