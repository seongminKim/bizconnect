<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-02
 * Time: 오후 1:44
 */

namespace Eguana\BizConnect\Cron\Send\Order;

use Eguana\BizConnect\Cron\Send\AbstractSender;
use Eguana\BizConnect\Model\Order\OrderDataIntegration;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Api\BizDataRepositoryInterface;

class OrderBind extends AbstractSender
{

    /**
     * @var OrderDataIntegration
     */
    private $orderDataIntegration;
    /**
     * @var Json
     */
    private $json;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        OrderDataIntegration $orderDataIntegration,
        Json $json,
        BizDataRepositoryInterface $bizDataRepository
    ) {
        parent::__construct($scopeConfig, $bizDataRepository);
        $this->orderDataIntegration = $orderDataIntegration;
        $this->json = $json;
    }

    public function execute()
    {
        if ($this->orderInterfaceIsEnable()) {
            $bindData = $this->orderDataIntegration->bindOrderData();
            $this->orderDataIntegration->saveBizData($bindData);
            $this->orderDataIntegration->updateOrderItemStatus($bindData);
        }
    }
}