<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-05
 * Time: 오후 1:35
 */

namespace Eguana\BizConnect\Cron\Send\Rma;

use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Cron\Send\AbstractSender;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Eguana\BizConnect\Model\Rma\RmaDataIntegration;
use Eguana\BizConnect\Model\Rma\Send;

class RmaSend extends AbstractSender
{

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var RmaDataIntegration
     */
    private $rmaDataIntegration;
    /**
     * @var Send
     */
    private $send;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        BizDataRepositoryInterface $bizDataRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Json $json,
        RmaDataIntegration $rmaDataIntegration,
        Send $send
    ) {
        parent::__construct($scopeConfig, $bizDataRepository);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->json = $json;
        $this->rmaDataIntegration = $rmaDataIntegration;
        $this->send = $send;
    }

    public function execute()
    {
        if ($this->rmaInterfaceIsEnable()) {
            $rmaData = $this->getRmaBizData();
            $requestData = $this->json->serialize($rmaData);

            $result = $this->send->sendData($requestData);

            try {
                $this->rmaDataIntegration->updateBizStatus($result, $rmaData);
                $message = 'SUCCESS';
            } catch (CouldNotSaveException $e) {
                $message = $e->getMessage();
            }
            return $message;
        }
    }

    private function getRmaBizData()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', '00')
            ->addFilter('type', 'RMA')
            ->create();

        $rmaBizData = $this->bizDataRepository->getList($searchCriteria);
        $rmaData = [];

        foreach ($rmaBizData->getItems() as $rma) {
            $rmaData[$rma['entity_id']] = $this->json->unserialize($rma['biz_data']);
        }

        return $rmaData;
    }
}