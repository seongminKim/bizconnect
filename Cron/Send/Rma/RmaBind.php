<?php
/**
 * Created by Eguana.
 * User: Glenn
 * Date: 2019-12-04
 * Time: 오전 9:06
 */

namespace Eguana\BizConnect\Cron\Send\Rma;

use Eguana\BizConnect\Api\BizDataRepositoryInterface;
use Eguana\BizConnect\Cron\Send\AbstractSender;
use Eguana\BizConnect\Model\Rma\RmaDataIntegration;
use Magento\Framework\App\Config\ScopeConfigInterface;

class RmaBind extends AbstractSender
{
    /**
     * @var RmaDataIntegration
     */
    private $rmaDataIntegration;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        BizDataRepositoryInterface $bizDataRepository,
        RmaDataIntegration $rmaDataIntegration
    ) {
        parent::__construct($scopeConfig, $bizDataRepository);
        $this->rmaDataIntegration = $rmaDataIntegration;
    }

    public function execute()
    {
        if ($this->rmaInterfaceIsEnable()) {
            $bindData = $this->rmaDataIntegration->bindRmaData();
            $this->rmaDataIntegration->saveBizData($bindData);
            $this->rmaDataIntegration->updateRmaItemStatus($bindData);
        }
    }
}